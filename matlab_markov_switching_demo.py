import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt

import os
os.system('clear')


# semianual freq
cpi = pd.read_excel("demo_stuff/Markov_switching_demo/CPI_AUC_SA_NSA.xls",
                    index_col = "observation_date")

print(cpi)

# get log retruns
cpi['log_ret'] = 100 * 2* (np.log(cpi.iloc[:,0]) - np.log(cpi.iloc[:,0].shift(1)))

# log returns and differences
logreturn = cpi["log_ret"].values
Dy = logreturn[1:] - logreturn[:-1]

# remove the nan values
Dy = Dy[1:]
logreturn = logreturn[1:]

# get T
T = len(Dy)


# Pick starting values based (somewhat) on data
p = [0.9, 0.1]
mu = [np.mean(Dy), np.mean(Dy)]
sigma = [0.9*np.std(Dy), 1.1*np.std(Dy)]



def L(v, Dy):
    nLL = -1 * np.sum(np.log(abs(v[0]) * norm.pdf(Dy, v[1], abs(v[3])) + abs((1-v[0])) * norm.pdf(Dy, v[2], abs(v[4]))))
    return nLL

starting_vals = [p[0], mu[0], mu[1], sigma[0], sigma[1]]

starting_neg_LL = L(v=starting_vals, Dy=Dy)
print(f"Starting negative LL:\n{starting_neg_LL}")


################################################################################
########################## UNCONSTRAINED OPTIMIZATION ##########################
################################################################################
# unconstrained optimization of the negative_LL
'''
comparison to matlab fminunc
https://nl.mathworks.com/help/optim/ug/fminunc.html
default_algo = Quasi-Newton
'''
res_unc = minimize(L, starting_vals, method = "BFGS", args = Dy)
ML_parameters_unc = res_unc.x
ML_NegativeLogL_unc = res_unc.fun
print(f"\nML parameters for unconstrained version:\n{ML_parameters_unc}")
print(f"\nML negative loglikelihood:\n{ML_NegativeLogL_unc}")


################################################################################
############################ CONSTRAINED OPTIMIZATION ##########################
################################################################################
# commented out for speed

# # constrained optimization of the negative_LL
# '''
# comparison to matlab fmincon:
# https://nl.mathworks.com/help/optim/ug/fmincon.html
# default_algo = 'interior-point'
# '''
# # define bounds for each parameters
# bounds = ((0,1), (-100,100), (-100, 100), (0,100), (0,100))
#
# # do a constrained minimization with a trust region (interior-point method) to
# # keep same logic as in matlab
# res_con = minimize(L, starting_vals, method = "trust-constr", args = Dy, bounds = bounds)
# ML_parameters_con = res_con.x
# ML_NegativeLogL_con = res_con.fun
# print(f"\nML parameter for constrained version:\n{ML_parameters_con}")
# print(f"ML neg_LL for constrained version:\n{ML_NegativeLogL_con}")


################################################################################
################################################################################
#################### SECOND PART OF MATLAB CODE ################################
################################################################################
################################################################################
'''
For now leave the EM part, I will focus first on the LikeliHood with the
whole MarkovSwitch part.

Now we go to the second part, I dont know why he removes 1 observation :(
'''

# reload the data. here he has one more step for y and Dy
# semianual freq
cpi = pd.read_excel("demo_stuff/Markov_switching_demo/CPI_AUC_SA_NSA.xls",
                    index_col = "observation_date")

# get log retruns
cpi['log_ret'] = 100 * 2* (np.log(cpi.iloc[:,0]) - np.log(cpi.iloc[:,0].shift(1)))

# log returns and differences
logreturn = cpi["log_ret"].values
y = logreturn[1:] - logreturn[:-1]   # <-- here y and Dy are different from above
Dy = y[1:] - y[:-1]

# remove the nan values
y = y[1:]
Dy = Dy[1:]
logreturn = logreturn[1:]

# get T
T = len(y)


'''
Now lets define the two functions he has for nLL with Hamilton
'''

def Hamilton_filter(p11, p22, mu1, mu2, sigma1, sigma2, y):
    '''
    Implementation of 2 state Hamilton filter from the Matlab Code
    '''
    T = len(y)

    # build transition matrix from p11 and p22
    P = np.array([[p11, 1-p22],
                  [1-p11, p22]])



    # create all the storing matrices
    likelihood = np.zeros(shape = (P.shape[0], T))
    filteredxi = np.zeros(shape = (P.shape[0], T))
    predictedxi = np.zeros(shape = (P.shape[0], T))

    # initialise by being in state 1???? <-- Lec 3 slide 46
    # first element is the unconditional probability of being in state 1
    # second element is the same for state 2
    # predictedxi[:,0] = [(1-p22) / (2-p11-p22), (1-p11)/(2-p11-p22)]

    # # initialise at state 1 for sure!
    # predictedxi[:,0] = np.dot(P, [1,0])
    # # initialise at state 2 for sure!
    predictedxi[:,0] = np.dot(P, [0,1])


    for i in range(0, T):
        if i == 0:
            likelihood[:,i] = [norm.pdf(y[0], mu1, sigma1), norm.pdf(y[0], mu2, sigma2)]
            filteredxi[:,i] = np.multiply(predictedxi[:,i], likelihood[:,i]) / (np.dot(np.ones(shape = (1,2)), np.multiply(predictedxi[:,i],likelihood[:,i])))
            predictedxi[:,i+1]= np.dot(P, filteredxi[:,i])



        else:
            likelihood[:,i] = [norm.pdf(y[:i+1], mu1, sigma1)[-1], norm.pdf(y[:i+1], mu2, sigma2)[-1]]
            filteredxi[:,i] = np.multiply(predictedxi[:,i], likelihood[:,i]) / (np.dot(np.ones(shape = (1,2)), np.multiply(predictedxi[:,i],likelihood[:,i])))
            if i+1 != T:
                predictedxi[:,i+1]= np.dot(P, filteredxi[:,i])


    return filteredxi, predictedxi



def NegativeLogLikelihood(parameter_vector, y):
    '''
    this function will also apply the Hamilton Filter
    '''

    p11 = parameter_vector[0]
    p22 = parameter_vector[1]
    mu1 = parameter_vector[2]
    mu2 = parameter_vector[3]
    sigma1 = parameter_vector[4]
    sigma2 = parameter_vector[5]

    # get the output of the Hamilton filter, aka the predictions
    filteredxi, predictedxi = Hamilton_filter(p11=p11,
                                              p22=p22,
                                              mu1=mu1,
                                              mu2=mu2,
                                              sigma1=sigma1,
                                              sigma2=sigma2,
                                              y=y)


    # collect the row vector of log likelihood per observation
    neg_LL = -1 * np.sum(np.log(np.multiply(predictedxi[0,:], norm.pdf(y, mu1, sigma1)) + np.multiply(predictedxi[1,:], norm.pdf(y, mu2, sigma2))))

    return neg_LL



starting_vals_hamilton = [0.8, 0.8, np.mean(Dy), np.mean(Dy), 0.5*np.std(Dy), 2*np.std(Dy)]

neg_LL = NegativeLogLikelihood(parameter_vector = starting_vals_hamilton, y = Dy)
print(f"starting neg LL for Hamilton:\t{neg_LL}")


# now do unconstrained and constrained minimization
# unconstrained optimization of the negative_LL
'''
comparison to matlab fminunc
https://nl.mathworks.com/help/optim/ug/fminunc.html
default_algo = Quasi-Newton
'''
# res_unc = minimize(NegativeLogLikelihood, x0=starting_vals_hamilton, method = "BFGS", args = Dy)
# BFGS fails here, so I tried Nelder-Mead
res_unc = minimize(NegativeLogLikelihood, x0=starting_vals_hamilton, method = "Nelder-Mead", args = Dy)
ML_parameters_unc = res_unc.x
ML_NegativeLogL_unc = res_unc.fun
print(f"\nML parameters for unconstrained version:\n{ML_parameters_unc}")
print(f"\nML negative loglikelihood:\n{ML_NegativeLogL_unc}")



# constrained optimization of the negative_LL
'''
comparison to matlab fmincon:
https://nl.mathworks.com/help/optim/ug/fmincon.html
default_algo = 'interior-point'
'''
# define bounds for each parameters
bounds = ((0,1), (0,1), (-100,100), (-100, 100), (0,100), (0,100))

# do a constrained minimization with a trust region (interior-point method) to
# keep same logic as in matlab
res_con = minimize(NegativeLogLikelihood,
                   x0=starting_vals_hamilton,
                   method = "SLSQP",
                   args = Dy,
                   bounds = bounds)
ML_parameters_con = res_con.x
ML_NegativeLogL_con = res_con.fun
print(f"\nML parameter for constrained version:\n{ML_parameters_con}")
print(f"\nML neg_LL for constrained version:\n{ML_NegativeLogL_con}")


def calculate_SE(func, est_params, args):
    Hfun = nd.Hessian(func, method = "backward")
    h = Hfun(est_params, args)
    return np.sqrt(np.diag(np.linalg.inv(h)))


se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                            est_params = res_con.x,
                            args = Dy)

df_constr_params = pd.DataFrame(index = ["p11", "p22", "mu1", "mu2", "sigma1", "sigm2"])
df_constr_params["ML_est"] = res_con.x
df_constr_params["ML_SE"] = se_ml_params
print(df_constr_params)


################################################################################
############################# OPTIMIZED FILTER PLOT ############################
################################################################################
# Run the Hamilton filter with the "Optimized parameters from ML" and see wazzup
filteredxi, predictedxi = Hamilton_filter(p11 = ML_parameters_unc[0],
                                          p22 = ML_parameters_unc[1],
                                          mu1 = ML_parameters_unc[2],
                                          mu2 = ML_parameters_unc[3],
                                          sigma1 = ML_parameters_unc[4],
                                          sigma2 = ML_parameters_unc[5],
                                          y = Dy)


def Hamilton_smoother(ML_estimators, y):

    # extract the estimated params
    p11 = ML_estimators[0]
    p22 = ML_estimators[1]
    mu1 = ML_estimators[2]
    mu2 = ML_estimators[3]
    sigma1 = ML_estimators[4]
    sigma2 = ML_estimators[5]

    # run the optimal Hamilton filter to get the filtered and predicted xi
    filteredxi, predictedxi = Hamilton_filter(p11, p22, mu1, mu2, sigma1, sigma2, y)

    # construct the "estimated P" from the estimated params I guess???
    P = np.array([[ML_estimators[0], 1-ML_estimators[1]],
                  [1-ML_estimators[0], ML_estimators[1]]])


    # construct the saving matrix
    T = predictedxi.shape[1]
    smoothedxi = np.zeros(shape=predictedxi.shape)

    # go backwards in time. Set last obs to be same in all cases, cuz there is
    # no more future data.
    for i in range(T):
        t = T-i
        if t > T -1:
            # which1 should it be here? :D
            smoothedxi[:,t-1] = predictedxi[:,T-1]
            # smoothedxi[:,t-1] = filteredxi[:,T-1]
        else:
            # here I thought it is the other way around
            # smoothedxi[:,t-1] = np.multiply(filteredxi[:,t-1], np.dot(P.T, np.divide(predictedxi[:,t],filteredxi[:,t])))
            smoothedxi[:,t-1] = np.multiply(predictedxi[:,t-1], np.dot(P.T, np.divide(filteredxi[:,t],predictedxi[:,t])))


    return smoothedxi


smoothedxi = Hamilton_smoother(ML_parameters_con, Dy)
# print(smoothedxi)

def filtered_plots(y, filteredxi, smoothedxi, bool_plot = False):
    '''
    funciton to plot the filtered points and log/probabilities
    for being in state 1/2

    y is Dy in the main example
    '''

    fig = plt.figure()
    plt.scatter(x = range(len(y)),
                y = y,
                label = "Jump in infl")
    plt.plot(range(len(filteredxi[1,:])),
            filteredxi[1,:],
            label = 'prob high vol state',
            color = 'r')

    plt.plot(range(len(smoothedxi[1,:])),
            smoothedxi[1,:],
            label = 'smoothed prob high vol state',
            color = 'g')

    plt.legend()
    plt.savefig("images/infl_demo.png")



    fig = plt.figure()
    plt.scatter(x = range(len(y)),
                y = y,
                label = "Jump in infl")
    plt.plot(range(len(filteredxi[1,:])),
            np.log(filteredxi[1,:]),
            label = 'Logprob high vol state',
            color = 'r')
    plt.plot(range(len(smoothedxi[1,:])),
            np.log(smoothedxi[1,:]),
            label = 'Smoothed Logprob high vol state',
            color = 'g')
    plt.legend()
    plt.savefig("images/log_infl_demo.png")

    if bool_plot:
        plt.show()


filtered_plots(y = Dy, filteredxi = filteredxi, smoothedxi = smoothedxi, bool_plot = True)
