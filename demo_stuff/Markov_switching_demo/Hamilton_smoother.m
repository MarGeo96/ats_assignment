function [ smoothedxi ] = Hamilton_smoother(p11,p22,mu,sigma,y)

% Extract length of data
T = size(y,2);

% Build transition matrix from p11 and p22
P   = [ p11 , 1-p22 ; 1-p11 , p22];

[ filteredxi , predictedxi ] = Hamilton_filter(p11,p22,mu,sigma,y);

for j=0:(T-1)
    t=T-j;
    if t>T-1
%         smoothedxi(:,t)=filteredxi(:,T);
        smoothedxi(:,t)=predictedxi(:,T);    
    else


%         smoothedxi(:,t) = filteredxi(:,t).*P'*(predictedxi(:,t+1)./filteredxi(:,t+1));
        smoothedxi(:,t) = predictedxi(:,t).*P'*(filteredxi(:,t+1)./predictedxi(:,t+1));
    end
end
% Close the function
end

