import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import os
os.system('cls')


def Kalman_Filter_Mdim(F, Q, H, y):
    
# =============================================================================
#     
#     '''
#     We start by extracting the values of the paramters.
#     '''
#     
#       # extract the values for H and contruct the matrix
#     h_values = parameter_vector[5:9]
#     
#     H = np.identity(3)
#     H = np.insert(H, 0, h_values, axis = 0)
#     
#     # extract and construct the F matrix
#     F = np.diag(parameter_vector[0:5])
#     
#     # extract the values for Q and contruct the matrix
#     q_values = parameter_vector[9:]
# 
#     q_vals = np.array(q_values)
#     q_vals = np.insert(q_values, 0, 1)
#     Q = np.diag(q_vals)
# =============================================================================
    
    T = y.shape[0]
    ncol = y.shape[1]
    
    xi0 = F * np.zeros(ncol)
    predictedxi = np.empty(T, ncol, 1)
    xi = np.zeros(T, ncol, 1)
    
    P0 = np.identity(ncol) * 10**6
    predictedP = np.empty((T, ncol, ncol))
    P = np.empty((T, ncol, ncol))

    for t in range(T):
        if t == 0:
            # initialize xi at 0
            predictedxi[t,:,:] = xi0

            # start at very big P
            predictedP[t,:,:] = np.dot( np.dot(F, P0), F.T ) + Q

        else:
            xi_pred = np.dot(F, xi[t-1])
            predictedxi[t,:,:] = xi_pred

            P_pred = np.dot(np.dot(F, P[t-1]), np.transpose(F)) + Q
            predictedP.append(P_pred)


        variance_data = np.linalg.inv(np.dot(np.dot(np.transpose(H), P_pred), H))
        
        deviation = y[t, :] - np.dot(np.transpose(H), xi_pred)

        xi[t,:,:] = xi_pred + np.dot( np.dot(np.dot(P_pred, H), variance_data), deviation)

        P[t,:,:]= P_pred - np.dot( np.dot(np.dot(P_pred, H), variance_data), np.dot(H.T, P_pred) )

    return xi, P, predictedxi, predictedP

def Kalman_Filter_MdimQ2e(F, Q, R, y):
    
# =============================================================================
#     
#     '''
#     We start by extracting the values of the paramters.
#     '''
#     
#       # extract the values for H and contruct the matrix
#     h_values = parameter_vector[5:9]
#     
#     H = np.identity(3)
#     H = np.insert(H, 0, h_values, axis = 0)
#     
#     # extract and construct the F matrix
#     F = np.diag(parameter_vector[0:5])
#     
#     # extract the values for Q and contruct the matrix
#     q_values = parameter_vector[9:]
# 
#     q_vals = np.array(q_values)
#     q_vals = np.insert(q_values, 0, 1)
#     Q = np.diag(q_vals)
# =============================================================================
    
    T = y.shape[0]
    ncol = y.shape[1]
    
    xi0 = np.dot(F, np.zeros(ncol))
    predictedxi = np.empty((T, ncol,))
    xi = np.zeros((T, ncol))
    
    P0 = np.identity(ncol) * 10**6
    predictedP = np.empty((T, ncol, ncol))
    P = np.empty((T, ncol, ncol))


    for t in range(T):
        if t == 0:
            # initialize xi at 0
            xi_pred = xi0
            predictedxi[t,:] = xi_pred

            # start at very big P
            predictedP[t,:,:] = np.dot( np.dot(F, P0), F.T ) + Q

        else:
            xi_pred = np.dot(F, xi[t-1,:])
            predictedxi[t,:] = xi_pred

            P_pred = np.dot(np.dot(F, P[t-1]), F.T) + Q
            predictedP[t,:,:] = P_pred


        variance_data = np.linalg.inv(predictedP[t,:,:] + R)
        
        deviation = y[t, :] - predictedxi[t,:]

        xi[t,:] = predictedxi[t,:] + np.dot( np.dot(predictedP[t,:,:], variance_data), deviation)

        P[t,:,:]= predictedP[t,:,:] - np.dot( np.dot(predictedP[t,:,:], variance_data), predictedP[t,:,:] )
        P[t,:,:] = (P[t,:,:] + P[t,:,:].T) / 2

    return xi, P, predictedxi, predictedP



def Kalman_Smoother_Mdim (F, Q, H, y):

    T = y.shape[0]
    ncol = y.shape[1]

# =============================================================================
#     '''
#     We start by extracting the values of the paramters.
#     '''
#     
#      # extract the values for H and contruct the matrix
#     h_values = parameter_vector[5:9]
#     
#     H = np.identity(4)
#     H = np.insert(H, 0, h_values, axis = 0)
#     
#     # extract and construct the F matrix
#     F = np.diag(parameter_vector[0:5])
#     
#     # extract the values for Q and contruct the matrix
#      q_values = parameter_vector[9:]
# 
#     q_vals = np.array(q_values)
#     q_vals = np.insert(q_values, 0, 1)
#     Q = np.diag(q_vals)
# =============================================================================



    # run optimal Kalman Filter results
    xi, P, predictedxi, predictedP = Kalman_Filter_Mdim(F= F, Q= Q, H= H, y= y)
    
    xi0 = F * np.zeros(ncol)    
    P0 = np.identity(ncol) * 10**6

    smoothedxi = np.empty((T, 1, ncol))
    smoothedP = np.empty((T, 1, ncol))

    for j in range(T):
        t = T - j - 1
        eq1 = np.dot(P[t,:,:], F.T)
        eq2 = np.dot( eq1, np.linalg.inv(predictedP[t+1,:,:]))
        eq3 = np.dot( eq2, (smoothedxi[t+1,:,:] - predictedxi[t+1,:,:]))
        smoothedxi[t,:,:] = xi[t,:,:] + eq3
        
        eq3 = np.dot( eq2, (predictedP[t+1,:,:] - smoothedP[t+1,:,:]) )
        eq4 = np.dot( eq3, np.linalg.inv(predictedP[t+1,:,:]) )
        smoothedP[t,:,:] = P[t,:,:] - np.dot( np.dot( eq4, F ), P[t,:,:])

    xi0_out = xi0 +  np.dot( np.dot( np.dot( P0, F.T), np.linalg.inv(predictedP[0,:,:])), (smoothedxi[0,:,:] - predictedxi[0,:,:]))
    P0_out = P0 - np.dot( np.dot( np.dot( np.dot( np.dot( np.dot(P0, F.T), np.linalg.inv(predictedP[0,:,:])), (predictedP[0,:,:] - smoothedP[0,:,:])), np.linalg.inv(predictedP[0,:,:])), F), P0)
    
    
    smoothedPcross = np.empty((T, ncol, ncol))
    
    for t in range(T):
        if t == 0:
            smoothedPcross[t,:,:] = np.dot( np.dot( np.dot(smoothedP[t,:,:], np.linalg.inv(predictedP[t,:,:])), F), P0)
        
        else:
            smoothedPcross[t,:,:] = np.dot( np.dot( np.dot(smoothedP[t,:,:], np.linalg.inv(predictedP[t,:,:])), F), P[t-1,:,:])
      
    return smoothedxi, smoothedP, smoothedPcross, xi0_out, P0_out
         
            

def Kalman_Smoother_MdimQ2e (F, Q, R, y):
    T = y.shape[0]
    ncol = y.shape[1]

    # run optimal Kalman Filter results
    xi, P, predictedxi, predictedP = Kalman_Filter_MdimQ2e(F= F, Q= Q, R= R, y= y)
    
    xi0 = np.dot(F, np.zeros(ncol))   
    P0 = np.identity(ncol) * 10**6

    smoothedxi = np.empty((T, ncol))
    smoothedP = np.empty((T, ncol, ncol))
    
    smoothedxi[T-1,:] = xi[T-1,:]
    smoothedP[T-1,:,:] = P[T-1,:,:]

    for j in (range(T)):
        t = T - j - 2
        smoothedxi[t,:] = xi[t,:] + \
        np.dot( np.dot( np.dot(P[t,:,:], F.T), np.linalg.inv(predictedP[t+1,:,:])), (smoothedxi[t+1,:] - predictedxi[t+1,:]))
        smoothedP[t,:,:] = P[t,:,:] - np.dot( np.dot( np.dot( np.dot( np.dot( np.dot( P[t,:,:], F.T ), np.linalg.inv(predictedP[t+1,:,:]) ), (predictedP[t+1,:,:] - smoothedP[t+1,:,:]) ), np.linalg.inv(predictedP[t+1,:,:]) ), F ), P[t,:,:])

    xi0_out = xi0 +  np.dot( np.dot( np.dot( P0, F.T), np.linalg.inv(predictedP[0,:,:])), (smoothedxi[0,:] - predictedxi[0,:]))
    P0_out = P0 - np.dot( np.dot( np.dot( np.dot( np.dot( np.dot(P0, F.T), np.linalg.inv(predictedP[0,:,:])), (predictedP[0,:,:] - smoothedP[0,:,:])), np.linalg.inv(predictedP[0,:,:])), F), P0)
    
    smoothedPcross = np.empty((T, ncol, ncol))
    
    for t in range(T):
        if t == 0:
            smoothedPcross[t,:,:] = np.dot( np.dot( np.dot(smoothedP[t,:,:], np.linalg.inv(predictedP[t,:,:])), F), P0)
        
        else:
            smoothedPcross[t,:,:] = np.dot( np.dot( np.dot(smoothedP[t,:,:], np.linalg.inv(predictedP[t,:,:])), F), P[t-1,:,:])        
    
    return smoothedxi, smoothedP, smoothedPcross, xi0_out, P0_out
    
