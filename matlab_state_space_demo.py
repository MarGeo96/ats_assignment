import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import os
os.system('clear')


# semianual freq
cpi = pd.read_excel("demo_stuff/Markov_switching_demo/CPI_AUC_SA_NSA.xls",
                    index_col = "observation_date")

print(cpi)

# get log retruns
cpi['log_ret'] = 100 * 2* (np.log(cpi.iloc[:,0]) - np.log(cpi.iloc[:,0].shift(1)))

# log returns and differences
logreturn = cpi["log_ret"].values[1:]

# de-mean the series
mu = np.mean(logreturn)
y = logreturn - mu

T = len(y)


# fig = plt.figure()
# plt.plot(cpi.index[1:], mu+y)
# plt.ylabel("%")
# plt.show()

# all gucci so far

phi = 1
sigma_eta_squared = 1/2
sigma_epsilon_squared = 1/100
parameter_vector = [phi, sigma_eta_squared, sigma_epsilon_squared]


def Kalman_Filter(parameter_vector, y):
    '''
    This function runs the Kalman filter for the scalar AR(1) model plus
    noise with a diffuse prior (roughly)
    '''

    T = len(y)

    # Extract the stuff we need from the input arguments
    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    # The Kalman filter for AR(1) + noise

    # this is for a singel column
    predictedxi = np.zeros(T)
    predictedP = np.zeros(T)
    xi = np.zeros(T)
    P = np.zeros(T)
    for t in range(T):
        # diffuse initialization
        if t == 0:
            predictedxi[t] = 0
            predictedP[t] = 10**6
        else:
            predictedxi[t] = np.dot(F, xi[t-1])
            predictedP[t] = np.dot(np.dot(F, P[t-1]), F) + Q


        xi[t] = predictedxi[t] + np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), (y[t]-predictedxi[t]))
        P[t] = predictedP[t] - np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), predictedP[t])

    return xi, P, predictedxi, predictedP

    # predictedxi = np.zeros(T)
    # predictedP = np.zeros(T)
    # xi = np.zeros(T)
    # P = np.zeros(T)
    # for t in range(T):
    #     # diffuse initialization
    #     if t == 0:
    #         predictedxi[t] = 0
    #         predictedP[t] = 10**6
    #     else:
    #         predictedxi[t] = np.dot(F, xi[t-1])
    #         predictedP[t] = np.dot(np.dot(F, P[t-1]), F.T) + Q
    #
    #
    #     xi[t] = predictedxi[t] + np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), (y[t]-predictedxi[t]))
    #     P[t] = predictedP[t] - np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), predictedP[t])
    #
    # return xi, P, predictedxi, predictedP


xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector, y = y)
# all gucci up until now

fig = plt.figure()
plt.scatter(cpi.index[1:], mu+y, color = "k")
plt.plot(cpi.index[1:], mu+xi, color = "g")
plt.plot(cpi.index[1:], mu+y+2*np.sqrt(P), color = "r")
plt.plot(cpi.index[1:], mu+y-2*np.sqrt(P), color = "r")
plt.legend()
# plt.show()


################################################################################
############# ESTIMATE PARAMS WITH MAXIMUM LIKELIHOOD ##########################
################################################################################
dy = y[1:] - y[:-1]
startingvalues = [1, 1/3*np.var(y), 2/3*np.var(y)]


def NegativeLogLikelihood(parameter_vector, y):
    '''
    this function will also apply the Hamilton Filter
    '''

    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector, y)

    T = len(y)

    Sigma = np.zeros(T)
    mu = np.zeros(T)
    LL = np.zeros(T)

    for t in range(T):
        Sigma[t] = predictedP[t]+R
        mu[t] = predictedxi[t]


        # for single columns we have
        LL[t] = np.log( 1 / np.sqrt( 2*np.pi*Sigma[t]) * np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , Sigma[t]**(-1)), y[t]-mu[t]) ))

        # HERE we have for multiple regressors situation, I belive it is the second line, but will see
        # LL[t] = np.log( 1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) * np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , np.linalg.inv(Sigma[t])), y[t]-mu[t]) ))
        # LL[t] = np.log( np.dot(1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) , np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , np.linalg.inv(Sigma[t])), y[t]-mu[t]) )))


    return -1 * np.sum(LL)

startingvalues = [1, 1/3*np.var(y), 2/3*np.var(y)]
nLL_starting = NegativeLogLikelihood(parameter_vector = startingvalues,
                                     y = y)
print(f"\nStarting negative log-likelihood:\t{nLL_starting}")

res_unc = minimize(NegativeLogLikelihood, startingvalues, method = "Nelder-Mead", args = y)
ML_parameters_unc = res_unc.x
ML_NegativeLogL_unc = res_unc.fun
print(f"\nML parameters for unconstrained version:\n{ML_parameters_unc}")
print(f"\nML negative loglikelihood:\n{ML_NegativeLogL_unc}")

# all gucci up untill now
# constrained optimization of the negative_LL
'''
comparison to matlab fmincon:
https://nl.mathworks.com/help/optim/ug/fmincon.html
default_algo = 'interior-point'
'''
# define bounds for each parameters
bounds = ((0,1), (0,2), (0,2))

# do a constrained minimization with a trust region (interior-point method) to
# keep same logic as in matlab
res_con = minimize(NegativeLogLikelihood,
                   x0=startingvalues,
                   method = "SLSQP",
                   args = y,
                   bounds = bounds)
ML_parameters_con = res_con.x
ML_NegativeLogL_con = res_con.fun
print(f"\nML parameter for constrained version:\n{ML_parameters_con}")
print(f"\nML neg_LL for constrained version:\n{ML_NegativeLogL_con}")


# add the standard errors:
def calculate_SE(func, est_params, args):
    Hfun = nd.Hessian(func, method = "backward")
    h = Hfun(est_params, args)
    return np.sqrt(np.diag(np.linalg.inv(h)))

se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                         est_params = res_con.x,
                         args = y)

# % [1;0.5;1/100] are phi, sigma_eta^2 and sigma_epsilon^2
df_constr_params = pd.DataFrame(index = ["phi", "sigma_eta_2", "sigma_epsilon_2"])
df_constr_params["ML_est"] = res_con.x
df_constr_params["ML_SE"] = se_ml_params
print(f"\nThe ML estimated params and their SE from the constrained optimization")
print(df_constr_params)

################################################################################
################## SMOOOTHER PART ##############################################
################################################################################

def Kalman_Smoother(parameter_vector, y):
    T = len(y)

    # Extract params
    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    # run optimal Kalman Filter results
    xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector, y)

    # for j=0:(T-1)
    # t = T-j;
    # % Set the last smoothed estimates equal to the filtered estimates
    # if t>T-1
    # smoothedxi(t) = xi(T);
    # smoothedP(t)  = P(T);
    # else
    # smoothedxi(t) = xi(t) + P(t) * F' * 1/predictedP(t+1) * ( smoothedxi(t+1) - predictedxi(t+1) ) ;
    # smoothedP(t)  = P(t)  - P(t) * F' * inv( predictedP(t+1) ) * ( predictedP(t+1) - smoothedP(t+1) ) * inv( predictedP(t+1) ) * F * P(t) ;

    smoothedxi = np.zeros(len(y))
    smoothedP = np.zeros(len(y))

    for j in range(T):
        t = T - j

        if t > T - 1:
            smoothedxi[t-1] = xi[t-1]
            smoothedP[t-1] = P[t-1]
        else:
            smoothedxi[t-1] = xi[t-1] + P[t-1] * F * 1 / predictedP[t] * (smoothedxi[t] - predictedxi[t])
            smoothedP[t-1] = P[t-1] - P[t-1] * F * 1/predictedP[t] * (predictedP[t] - smoothedP[t]) * 1/predictedP[t] * F * P[t-1]

    return smoothedxi, smoothedP

smoothedxi, smoothedP = Kalman_Smoother(res_con.x, y = y)
# print(smoothedxi)
# print(smoothedP)

# all gucci up until now


# run optimal Kalman Filter results
xi, P, predictedxi, predictedP = Kalman_Filter(res_con.x, y)


# Kalman filter RMSE vs a random walk
RMSE_vs_RW = np.var(y[1:] - predictedxi[1:]) / np.var(y[1:] - y[:-1])

# relative improvement
rel_improve = np.sqrt(np.var(y[1:] - predictedxi[1:])) / np.sqrt( np.var(y[1:] - y[:-1]))

print(f"\nRMSE vs RD is:\t{RMSE_vs_RW}")
print(f"\nThe relative improvement is:\t{rel_improve}")
