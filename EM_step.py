import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt

from state_space_util_mdim import *

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import os
os.system('cls')

def EM_step(parameter_vector, y):
    
    T = y.shape[0]
    ncol = y.shape[1]
    
    
    ### Start by extracting the values of the parameters ###
    h_values = parameter_vector[5:8]
    
    H = np.identity(3)
    H = np.insert(H, 0, h_values, axis = 0)
    
    # extract and construct the F matrix
    F = np.diag(parameter_vector[0:5])
    
    # extract the values for Q and contruct the matrix
    q_values = parameter_vector[9:]

    q_vals = np.array(q_values)
    q_vals = np.insert(q_values, 0, 1)
    Q = np.diag(q_values)
    
    ### E-step: Run the Kalman smoother ###
    smoothedXi, smoothedP, smoothedPcross, xi0_out, P0_out = Kalman_Smoother_Mdim(parameter_vector, y)
    
    
    numerator = np.empty(shape = (T, F.shape[0], F.shape[1]))
    denominator = np.empty(shape = (T, F.shape[0], F.shape[1]))
    ### Optimise over F. 
    #Note that we are now working with 3D arrays, i.e. a[t,x,y], where t is the time dimension
    for t in range(T):
        if t == 0:
            numerator[t,:,:] = np.outer(smoothedXi[t,:,:], xi0_out.T) + smoothedPcross[t,:,:]
        
        else:
            numerator[t,:,:] = np.outer(smoothedXi[t,:,:], smoothedXi[t-1,:,:].T) + smoothedPcross[t,:,:]
        
    for t in range(T):
        if t == 0:
            denominator[t,:,:] = np.outer(xi0_out, xi0_out.T) + P0_out
            
        else:
            denominator[t,:,:] = np.outer(smoothedXi[t-1,:,:], smoothedXi[t-1,:,:].T) + smoothedP[t-1,:,:]
    
    mean_num = numerator.mean(axis = (0))
    mean_denom = denominator.mean(axis = (0))
    F_out = np.dot(mean_num, np.linalg.inv(mean_denom))
    
    
    numerator = np.empty(shape = (T, H.shape[1], H.shape[0]))
    denominator = np.empty(shape = (T, F.shape[0], F.shape[1]))   
    ### Optimise over H
    
    for t in range(T):
        numerator[t,:,:] = np.out(y, smoothedXi[t,:,:].T)
        denominator[t,:,:] = np.out(smoothedXi[t,:,:], smoothedXi[t,:,:].T) + smoothedP[t,:,:]
            
    mean_num = numerator.mean(axis = (0))
    mean_denom = denominator.mean(axis = (0))
    Hprime_out = np.dot(mean_num, np.linalg.inv(mean_denom))

    del numerator, denominator, mean_num, mean_denom
    
    Q_estimate = np.empty(shape = (T, Q.shape[0], Q.shape[1]))
    ### Optimise over Q
    for t in range(T):
        if t == 0:
            sum1 = np.outer(xi0_out, smoothedXi[t,:,:].T) + smoothedPcross[t,:,:].T
            sum2 = np.outer(smoothedXi[t,:,:], xi0_out.T) + smoothedPcross[t,:,:]
            sum3 = np.outer(xi0_out, xi0_out.T) + P0_out
            Q_estimate[t,:,:] = np.dot(smoothedXi[t,:,:], smoothedXi[t,:,:].T) + smoothedP[t,:,:] - np.dot(F_out, sum1) - \
                np.dot(sum2, np.linalg.inv(F_out)) + np.dot(np.dot(F_out, sum3), np.linalg.inv(F_out))         
        else:
            sum1 = np.outer(smoothedXi[t-1,:,:], smoothedXi[t,:,:].T) + smoothedPcross[t,:,:].T
            sum2 = np.outer(smoothedXi[t,:,:], smoothedXi[t-1,:,:].T) + smoothedPcross[t,:,:]
            sum3 = np.outer(smoothedXi[t-1,:,:], smoothedXi[t-1,:,:].T) + smoothedP[t-1,:,:]
            Q_estimate[t,:,:] = np.outer(smoothedXi[t,:,:], smoothedXi[t,:,:].T) + smoothedP[t,:,:] - np.dot(F_out, sum1) - \
                np.dot(sum2, np.linalg.inv(F_out)) + np.dot(np.dot(F_out, sum3), np.linalg.inv(F_out)) 
                            
    Q_out = Q_estimate.mean(axis=(0))
    
    del sum1, sum2, sum3, Q_estimate
    
# =============================================================================
#     ### Optimise over R
#     R_estimate = np.empty(shape = (T, ncol, ncol))
#     for t in range(T):
#         sum1 = np.outer(smoothedXi[t,:,:], smoothedXi[t,:,:].T) + smoothedP[t,:,:]
#         R_estimate[t,:,:] = np.dot(y, y.T) - np.dot( np.dot(Hprime_out, smoothedXi[t,:,:]), y.T ) - \
#                         np.dot( np.dot(y, np.linalg.inv(smoothedXi[t,:,:])), np.linalg.inv(Hprime_out) ) + \
#                         np.dot( np.dot(Hprime_out, sum1), np.linalg.inv(Hprime_out) )
# 
# =============================================================================

def EM_stepQ2e(F, Q, R, y):
    
    T = y.shape[0]
    ncol = y.shape[1]
    
    
# =============================================================================
#     ### Start by extracting the values of the parameters ###
#     h_values = parameter_vector[5:8]
#     
#     H = np.identity(3)
#     H = np.insert(H, 0, h_values, axis = 0)
#     
#     # extract and construct the F matrix
#     F = np.diag(parameter_vector[0:5])
#     
#     # extract the values for Q and contruct the matrix
#     q_values = parameter_vector[9:]
# 
#     q_vals = np.array(q_values)
#     q_vals = np.insert(q_values, 0, 1)
#     Q = np.diag(q_values)
# =============================================================================
    
    ### E-step: Run the Kalman smoother ###
    smoothedXi, smoothedP, smoothedPcross, xi0_out, P0_out = Kalman_Smoother_MdimQ2e(F, Q, R, y)
    # print(smoothedXi)
    # raise SystemExit
    
    numerator = np.empty(shape = (T, F.shape[0], F.shape[1]))
    denominator = np.empty(shape = (T, F.shape[0], F.shape[1]))
    ### Optimise over F. 
    #Note that we are now working with 3D arrays, i.e. a[t,x,y], where t is the time dimension
    for t in range(T):
        if t == 0:
            numerator[t,:,:] = np.outer(smoothedXi[t,:], xi0_out) + smoothedPcross[t,:,:]
        
        else:
            numerator[t,:,:] = np.outer(smoothedXi[t,:], smoothedXi[t-1,:]) + smoothedPcross[t,:,:]
        
    for t in range(T):
        if t == 0:
            denominator[t,:,:] = np.outer(xi0_out, xi0_out.T) + P0_out
            
        else:
            denominator[t,:,:] = np.outer(smoothedXi[t-1,:], smoothedXi[t-1,:]) + smoothedP[t-1,:,:]
    
    mean_num = numerator.mean(axis = (0))
    mean_denom = denominator.mean(axis = (0))
    F_out = np.dot(mean_num, np.linalg.inv(mean_denom))
    
# =============================================================================
#     
#     numerator = np.empty(shape = (T, H.shape[1], H.shape[0]))
#     denominator = np.empty(shape = (T, F.shape[0], F.shape[1]))   
#     ### Optimise over H
#      
#     for t in range(T):
#         numerator[t,:,:] = np.out(y, smoothedXi[t,:,:].T)
#         denominator[t,:,:] = np.out(smoothedXi[t,:,:], smoothedXi[t,:,:].T) + smoothedP[t,:,:]
#             
#     mean_num = numerator.mean(axis = (0))
#     mean_denom = denominator.mean(axis = (0))
#     Hprime_out = np.dot(mean_num, np.linalg.inv(mean_denom))
# 
#     del numerator, denominator, mean_num, mean_denom
# =============================================================================
    
    Q_estimate = np.empty(shape = (T, Q.shape[0], Q.shape[1]))
    ### Optimise over Q
    for t in range(T):
        if t == 0:
            sum1 = np.outer(xi0_out, smoothedXi[t,:]) + smoothedPcross[t,:,:].T
            sum2 = np.outer(smoothedXi[t,:], xi0_out) + smoothedPcross[t,:,:]
            sum3 = np.outer(xi0_out, xi0_out) + P0_out
            Q_estimate[t,:,:] = np.outer(smoothedXi[t,:], smoothedXi[t,:]) + smoothedP[t,:,:] - np.dot(F_out, sum1) - \
                np.dot(sum2, F_out.T) + np.dot(np.dot(F_out, sum3), F_out.T)         
        else:
            sum1 = np.outer(smoothedXi[t-1,:], smoothedXi[t,:]) + smoothedPcross[t,:,:].T
            sum2 = np.outer(smoothedXi[t,:], smoothedXi[t-1,:]) + smoothedPcross[t,:,:]
            sum3 = np.outer(smoothedXi[t-1,:], smoothedXi[t-1,:]) + smoothedP[t-1,:,:]
            Q_estimate[t,:,:] = np.outer(smoothedXi[t,:], smoothedXi[t,:]) + smoothedP[t,:,:] - np.dot(F_out, sum1) - \
                np.dot(sum2, F_out.T) + np.dot(np.dot(F_out, sum3), F_out.T) 
                            
    Q_out = Q_estimate.mean(axis=(0))
    
    del sum1, sum2, sum3, Q_estimate
    
    ### Optimise over R
    R_estimate = np.empty(shape = (T, ncol, ncol))
    for t in range(T):
        sum1 = np.outer(smoothedXi[t,:], smoothedXi[t,:]) + smoothedP[t,:,:]
        R_estimate[t,:,:] = np.outer(y[t,:], y[t,:]) - np.outer(smoothedXi[t,:], y[t,:] ) - \
                         np.outer(y[t,:], smoothedXi[t,:]) + \
                         sum1
    R_out = R_estimate.mean(axis=(0)) 
                        
    
    return F_out, Q_out, R_out


