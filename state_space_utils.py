import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import os
os.system("clear")



def Kalman_Filter(parameter_vector, y):
    '''
    This function runs the Kalman filter for the scalar AR(1) model plus
    noise with a diffuse prior (roughly)
    '''

    T = len(y)

    # Extract the stuff we need from the input arguments
    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    # The Kalman filter for AR(1) + noise

    # this is for a singel column
    predictedxi = np.zeros(T)
    predictedP = np.zeros(T)
    xi = np.zeros(T)
    P = np.zeros(T)
    for t in range(T):
        # diffuse initialization
        if t == 0:
            predictedxi[t] = 0
            predictedP[t] = 10**6
        else:
            predictedxi[t] = np.dot(F, xi[t-1])
            predictedP[t] = np.dot(np.dot(F, P[t-1]), F) + Q


        xi[t] = predictedxi[t] + np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), (y[t]-predictedxi[t]))
        P[t] = predictedP[t] - np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), predictedP[t])

    return xi, P, predictedxi, predictedP

    # predictedxi = np.zeros(T)
    # predictedP = np.zeros(T)
    # xi = np.zeros(T)
    # P = np.zeros(T)
    # for t in range(T):
    #     # diffuse initialization
    #     if t == 0:
    #         predictedxi[t] = 0
    #         predictedP[t] = 10**6
    #     else:
    #         predictedxi[t] = np.dot(F, xi[t-1])
    #         predictedP[t] = np.dot(np.dot(F, P[t-1]), F.T) + Q
    #
    #
    #     xi[t] = predictedxi[t] + np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), (y[t]-predictedxi[t]))
    #     P[t] = predictedP[t] - np.dot(np.dot(predictedP[t], 1/(predictedP[t] + R)), predictedP[t])
    #
    # return xi, P, predictedxi, predictedP



def NegativeLogLikelihood(parameter_vector, y):
    '''
    this function will also apply the Hamilton Filter
    '''

    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector, y)

    T = len(y)

    Sigma = np.zeros(T)
    mu = np.zeros(T)
    LL = np.zeros(T)

    for t in range(T):
        Sigma[t] = predictedP[t]+R
        mu[t] = predictedxi[t]


        LL[t] = np.log( 1 / np.sqrt( 2*np.pi*Sigma[t]) * np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , Sigma[t]**(-1)), y[t]-mu[t]) ))

        # HERE we have for multiple regressors situation, I belive it is the second line, but will see
        # LL[t] = np.log( 1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) * np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , np.linalg.inv(Sigma[t])), y[t]-mu[t]) ))
        # LL[t] = np.log( np.dot(1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) , np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , np.linalg.inv(Sigma[t])), y[t]-mu[t]) )))
        # print(LL[t])

    return -1 * np.sum(LL)


def Kalman_Smoother(parameter_vector, y):
    T = len(y)

    # Extract params
    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    # run optimal Kalman Filter results
    xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector, y)


    smoothedxi = np.zeros(len(y))
    smoothedP = np.zeros(len(y))

    for j in range(T):
        t = T - j

        if t > T - 1:
            smoothedxi[t-1] = xi[t-1]
            smoothedP[t-1] = P[t-1]
        else:
            smoothedxi[t-1] = xi[t-1] + P[t-1] * F * 1 / predictedP[t] * (smoothedxi[t] - predictedxi[t])
            smoothedP[t-1] = P[t-1] - P[t-1] * F * 1/predictedP[t] * (predictedP[t] - smoothedP[t]) * 1/predictedP[t] * F * P[t-1]

    return smoothedxi, smoothedP


def state_space_prediction(step_prediction, y_observ, est_xi, est_phi):
    '''
    TODO: double check the 4 step ahead prediction. It seems a bit off!
    '''
    # save the initial unforecastable observations
    y_observ = list(y_observ)
    forecasts = y_observ[:step_prediction]

    # for idx, y in enumerate(y_observ[step_prediction - 1:-1]):
    #     # middle step is not needed in this version I think,
    #     # but for the next it might be
    #     pred_xi = est_phi ** (step_prediction) * est_xi[idx]
    #     pred_y = pred_xi
    #     forecasts.append(pred_y)

    # I think this1 is more correct
    # although the correlation of forecast 4 differs a bit
    for idx, y in enumerate(y_observ[:-step_prediction]):
        # middle step is not needed in this version I think,
        # but for the next it might be
        pred_xi = est_phi ** (step_prediction) * est_xi[idx]
        pred_y = pred_xi
        forecasts.append(pred_y)



    return forecasts


################################################################################
############################## MULTINOMIAL PART ################################
################################################################################





def Multi_Kalman_Smoother(parameter_vector, y):
    T = len(y)

    # Extract params
    F = parameter_vector[0]
    Q = parameter_vector[1]
    R = parameter_vector[2]

    # run optimal Kalman Filter results
    xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector, y)


    smoothedxi = np.zeros(len(y))
    smoothedP = np.zeros(len(y))

    for j in range(T):
        t = T - j

        if t > T - 1:
            smoothedxi[t-1] = xi[t-1]
            smoothedP[t-1] = P[t-1]
        else:
            smoothedxi[t-1] = xi[t-1] + P[t-1] * F * 1 / predictedP[t] * (smoothedxi[t] - predictedxi[t])
            smoothedP[t-1] = P[t-1] - P[t-1] * F * 1/predictedP[t] * (predictedP[t] - smoothedP[t]) * 1/predictedP[t] * F * P[t-1]

    return smoothedxi, smoothedP






def Multi_Kalman_Filter(parameter_vector, y):
    '''
    This function runs the Kalman filter for the scalar AR(1) model plus
    noise with a diffuse prior (roughly)
    '''

    # T = len(y)
    #
    # # Extract the stuff we need from the input arguments
    # F = parameter_vector[0]
    # Q = parameter_vector[1]
    # R = parameter_vector[2]
    #
    # # The Kalman filter for AR(1) + noise
    #
    # # this is for a singel column
    # predictedxi = np.zeros(T)
    # predictedP = np.zeros(T)
    # xi = np.zeros(T)
    # P = np.zeros(T)

    T = y.shape[0]

    # Extract the stuff we need from the input arguments
    # extract h
    h_vals = parameter_vector[5:9]
    # construct H matrix
    H = np.identity(4)
    H = np.insert(H, 0, h_vals, axis = 0)

    # construct f matrix
    F = np.diag(parameter_vector[0:5])

    # extract q and construct Q matrix
    q_vals = parameter_vector[9:]
    # q_vals.insert(0, 1)
    q_vals = np.array(q_vals)
    q_vals = np.insert(q_vals, 0, 1)
    Q = np.diag(q_vals)
    # print(Q)
    # exit()

    # print(f"H:\n{H}")
    # print(f"F\n{F}")
    # print(f"Q\n{Q}")


    # The Kalman filter for AR(4) + noise

    # now we have 5 xi
    # 5x5 P matrix, which has to be an array containing T 5x5 matrices
    # same for the two predxi and predP

    # predictedxi = np.zeros(shape = (y.shape[0], y.shape[1] + 1))
    # xi = np.zeros(shape = (y.shape[0], y.shape[1] + 1))


    predictedxi = []
    xi = []

    predictedP = []
    P = []

    for t in range(T):
        if t == 0:
            # initialize xi at 0
            pred_xi = [0, 0, 0, 0, 0]
            predictedxi.append(pred_xi)

            # start at very big P
            pred_P = np.identity(y.shape[1] + 1)
            pred_P = pred_P * 10**6
            predictedP.append(pred_P)

        else:
            pred_xi = np.dot(F, xi[t-1])
            predictedxi.append(pred_xi)

            pred_P = np.dot(np.dot(F, P[t-1]), F.T) + Q
            predictedP.append(pred_P)


        bracket_term = np.linalg.inv(np.dot(np.dot(H.T, pred_P), H))
        pred_diff = y[t, :] - np.dot(H.T, pred_xi)
        # print("**"*20)

        xi_new = pred_xi + np.dot( np.dot(np.dot(pred_P, H), bracket_term), pred_diff)
        # print(xi_new)

        P_new = pred_P - np.dot( np.dot(np.dot(pred_P, H), bracket_term), np.dot(H.T, pred_P) )
        # print(P_new)

        xi.append(xi_new)
        P.append(P_new)


    predictedxi = np.asarray(predictedxi, dtype=None)
    predictedP = np.asarray(predictedP, dtype=None)

    return xi, P, predictedxi, predictedP


def MultiNegativeLogLikelihood(parameter_vector, y):
    '''
    this function will also apply the Hamilton Filter
    '''

    xi, P, predictedxi, predictedP = Multi_Kalman_Filter(parameter_vector, y)

    T = y.shape[0]



    # Extract the stuff we need from the input arguments
    # extract h
    h_vals = parameter_vector[5:9]
    # construct H matrix
    H = np.identity(4)
    H = np.insert(H, 0, h_vals, axis = 0)

    # construct f matrix
    F = np.diag(parameter_vector[0:5])

    # extract q and construct Q matrix
    q_vals = parameter_vector[9:]
    # q_vals.insert(0, 1)
    q_vals = np.array(q_vals)
    q_vals = np.insert(q_vals, 0, 1)
    Q = np.diag(q_vals)

    # print(f"H:\n{H}")
    # print(f"F\n{F}")
    # print(f"Q\n{Q}")


    # Sigma = np.zeros(shape = y.shape)
    Sigma = []
    mu = np.zeros(shape = y.shape)
    LL = np.zeros(T)

    for t in range(T):

        # # these two have to be modified
        current_sigma =  np.dot(np.dot(H.T, predictedP[t,:]), H)
        # Sigma[t] = predictedP[t,:]

        # this has to be multiplied by H I think????
        mu[t] = np.dot(H.T, predictedxi[t,:])

        # LL[t] = np.log( 1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) * np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , Sigma[t]**(-1)), y[t]-mu[t]) ))

        # HERE we have for multiple regressors situation, I belive it is the second line, but will see
        # LL[t] = np.log( 1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) * np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , np.linalg.inv(Sigma[t])), y[t]-mu[t]) ))
        # LL[t] = np.log( np.dot(1 / np.sqrt( np.linalg.det(2*np.pi*Sigma[t])) , np.exp(-1/2 * np.dot(np.dot((y[t]-mu[t]).T , np.linalg.inv(Sigma[t])), y[t]-mu[t]) )))

        LL_1 = 1/np.sqrt(np.linalg.det(2*np.pi*current_sigma))


        LL_2 = np.exp(-0.5 * np.dot(np.dot((y[t] - mu[t]).T, np.linalg.inv(current_sigma)), (y[t] - mu[t])))



        dot_L = np.dot(LL_1, LL_2)
        LL[t] = np.log(dot_L)


    return -1 * np.sum(LL)
