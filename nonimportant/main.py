import os
import pandas as pd
import matplotlib.pyplot as plt
import statsmodels.api as sm
os.system('clear')


def plot_data(data, bool_plot = False):
    '''
    just plots the given data
    '''
    for col in data.columns:
        figure = plt.figure()
        data[col].plot()
        plt.ylabel(col)
        plt.xticks(rotation=30)
        plt.savefig(f"images/data_plot_{col}.png")

    if bool_plot:
        plt.show()


if __name__ == '__main__':

        # load data
        data = pd.read_csv("assignment/data/data.csv", index_col = "date")
        data.index = pd.DatetimeIndex(data.index, freq='QS')
        print(data)


        # get only "y3"
        industrial_growth = data.iloc[:,2]
        print(industrial_growth)


        # Fit the model
        mod_hamilton = sm.tsa.MarkovAutoregression(industrial_growth, k_regimes=2, order=1, switching_ar=False)
        res_hamilton = mod_hamilton.fit()

        # print the estimated coefficients
        print(res_hamilton.summary())



        fig, axes = plt.subplots(2, figsize=(7,7))
        ax = axes[0]
        ax.plot(res_hamilton.filtered_marginal_probabilities[0])
        ax.set(title='Filtered probability of recession')

        ax = axes[1]
        ax.plot(res_hamilton.smoothed_marginal_probabilities[0])
        ax.set(title='Smoothed probability of recession')

        plt.show()
