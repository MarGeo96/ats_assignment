%% Clear all data
clear
close all

% %% Load correct directory
% 
% % for Mac users, it should look like this:
% pad = '/Users/Rutger-Jan/Dropbox/EUR/ATSE/ATSE 2020-21/Assignment/sent out/matlab'
% 
% % for PC users, it should look like this
% %pad = 'C:/Users/61849rla/Dropbox/Teaching/Assignment/Q2a';
% 
% % load this directory
% cd(pad);
% format short

%% Load data

load('data')
dates = table2array(data(1:end,1))+datenum('31/12/1899','dd/mm/yyyy'); 
% column 1 contains date numbers
% we have to make a correction to the dates because Excel dates start counting at 1 Jan 1900
y = table2array(data(1:end,[2:4]))'; % columns 2-5 contain the real data
% we make sure y(t) is a column vector, so t runs towards the right

%% Plot series 1: Expenditure growth
figure
plot(dates,y(1,:),'-ks','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',10)
dateFormat = 'yy';
datetick('x',dateFormat)
startrow=datenum('1/1/1973','dd/mm/yyyy')
endrow=datenum('1/9/2020','dd/mm/yyyy')
axis([startrow,endrow,-inf,inf])
title('Annualised expenditure growth $(\%)$','Interpreter','latex')
ylabel('$\%$','FontSize',24,'FontWeight','bold','Color','k','Interpreter','latex') % y-axis label
set(gca,'FontSize',30)
set(gca,'FontName','Times New Roman')

%% Plot series 2: Employment growth
figure
plot(dates,y(2,:),'-ks','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',10)
dateFormat = 'yy';
datetick('x',dateFormat)
startrow=datenum('1/1/1973','dd/mm/yyyy')
endrow=datenum('1/9/2020','dd/mm/yyyy')
axis([startrow,endrow,-inf,inf])
title('Annualised employment growth $(\%)$','Interpreter','latex')
ylabel('$\%$','FontSize',24,'FontWeight','bold','Color','k','Interpreter','latex') % y-axis label
set(gca,'FontSize',30)
set(gca,'FontName','Times New Roman')

%% Plot series 3: Industrial prodution growth
figure
plot(dates,y(3,:),'-ks','MarkerFaceColor','k','MarkerEdgeColor','k','MarkerSize',10)
dateFormat = 'yy';
datetick('x',dateFormat)
startrow=datenum('1/1/1973','dd/mm/yyyy')
endrow=datenum('1/9/2020','dd/mm/yyyy')
axis([startrow,endrow,-inf,inf])
title('Annualised IP growth $(\%)$','Interpreter','latex')
ylabel('$\%$','FontSize',24,'FontWeight','bold','Color','k','Interpreter','latex') % y-axis label
set(gca,'FontSize',30)
set(gca,'FontName','Times New Roman')

