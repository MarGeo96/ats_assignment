import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt
import os

from markov_switch_utils import Hamilton_filter, NegativeLogLikelihood, filtered_plots


os.system('clear')


if __name__ == '__main__':
    # set up the whole data structure

    # semianual freq
    cpi = pd.read_excel("demo_stuff/Markov_switching_demo/CPI_AUC_SA_NSA.xls",
                        index_col = "observation_date")

    # get log retruns
    cpi['log_ret'] = 100 * 2* (np.log(cpi.iloc[:,0]) - np.log(cpi.iloc[:,0].shift(1)))

    # log returns and differences
    logreturn = cpi["log_ret"].values
    y = logreturn[1:] - logreturn[:-1]   # <-- here y and Dy are different from above
    Dy = y[1:] - y[:-1]

    # remove the nan values
    y = y[1:]
    Dy = Dy[1:]
    logreturn = logreturn[1:]

    # get T
    T = len(y)

    # set some starting values (copied from the MATLAB example)
    starting_vals_hamilton = [0.8, 0.8, np.mean(Dy), np.mean(Dy), 0.5*np.std(Dy), 2*np.std(Dy)]

    # calculate initial neg_LL to see if we can improve over it
    neg_LL = NegativeLogLikelihood(parameter_vector = starting_vals_hamilton, y = Dy)
    print(f"Starting neg LL for Hamilton:\n{neg_LL}")
    print("--"*20)


    ################## (UN)CONSTRAINED OPTIMIZATION ###################

    # now do unconstrained and constrained minimization
    # unconstrained optimization of the negative_LL
    '''
    comparison to matlab fminunc
    https://nl.mathworks.com/help/optim/ug/fminunc.html
    default_algo = Quasi-Newton
    '''
    print(f"\nUNCONSTRAINED OPTIMIZATION RESULTS")
    print("--"*20)
    # res_unc = minimize(NegativeLogLikelihood, x0=starting_vals_hamilton, method = "BFGS", args = Dy)
    # BFGS fails here, so I tried Nelder-Mead
    res_unc = minimize(NegativeLogLikelihood,
                       x0=starting_vals_hamilton,
                       method = "Nelder-Mead",
                       args = Dy)
    ML_parameters_unc = res_unc.x
    ML_NegativeLogL_unc = res_unc.fun


    print(f"\nML parameters for unconstrained version:\n{ML_parameters_unc}")
    print(f"\nML neg_LL for unconstrained version:\n{ML_NegativeLogL_unc}")



    # constrained optimization of the negative_LL
    '''
    comparison to matlab fmincon:
    https://nl.mathworks.com/help/optim/ug/fmincon.html
    default_algo = 'interior-point'
    '''
    print(f"\nCONSTRAINED OPTIMIZATION RESULTS")
    print("--"*20)
    # define bounds for each parameters
    bounds = ((0,1), (0,1), (-100,100), (-100, 100), (0,100), (0,100))

    # do a constrained minimization with a trust region (interior-point method) to
    # keep same logic as in matlab
    res_con = minimize(NegativeLogLikelihood,
                       x0=starting_vals_hamilton,
                       method = "SLSQP",
                       args = Dy,
                       bounds = bounds)
    ML_parameters_con = res_con.x
    ML_NegativeLogL_con = res_con.fun

    print(f"\nML parameter for constrained version:\n{ML_parameters_con}")
    print(f"\nML neg_LL for constrained version:\n{ML_NegativeLogL_con}")

    def calculate_SE(func, est_params, args):
        Hfun = nd.Hessian(func, method = "backward")
        h = Hfun(est_params, args)
        return np.sqrt(np.diag(np.linalg.inv(h)))


    se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                                est_params = res_con.x,
                                args = Dy)

    df_constr_params = pd.DataFrame(index = ["p11", "p22", "mu1", "mu2", "sigma1", "sigm2"])
    df_constr_params["ML_est"] = res_con.x
    df_constr_params["ML_SE"] = se_ml_params
    print(df_constr_params)



    ################################################################################
    ############################# OPTIMIZED FILTER PLOT ############################
    ################################################################################
    # Run the Hamilton filter with the "Optimized parameters from ML" and see wazzup
    filteredxi, predictedxi = Hamilton_filter(p11 = ML_parameters_unc[0],
                                              p22 = ML_parameters_unc[1],
                                              mu1 = ML_parameters_unc[2],
                                              mu2 = ML_parameters_unc[3],
                                              sigma1 = ML_parameters_unc[4],
                                              sigma2 = ML_parameters_unc[5],
                                              y = Dy)
    filtered_plots(y = Dy, filteredxi = filteredxi, bool_plot = True)
