import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
from scipy.optimize import least_squares
import numdifftools as nd
import matplotlib.pyplot as plt
import os

from markov_switch_utils import *


os.system('clc')


def calculate_SE(func, est_params, args):
    Hfun = nd.Hessian(func, method = "backward")
    h = Hfun(est_params, args)
    return np.sqrt(np.diag(np.linalg.inv(h)))

if __name__ == '__main__':
    # load the data of annualized cons, employment and industrial growth
    data = pd.read_csv("assignment/data/data.csv", index_col = "date")

    industrcial_prod = data.iloc[:,-1]
    print(f"\nIndustrial data:\n{industrcial_prod}")

    # industrcial_prod.plot()
    # plt.ylabel("annual industrial growth")
    # # plt.show()



    '''
    starting params: 6x
    p11     <-- depends on initialization eg. 0.8 (from MATLAB example)
    p22     <-- depends on initialization eg. 0.8 (from MATLAB example)
    mu1     <-- sample mean of series
    mu2     <-- sample mean of series
    sigma1  <-- sample std of series * 0.5
    sigma2  <-- sample std of series * 2
    '''

    p11 = 0.8
    p22 = 0.8
    mu1 = np.mean(industrcial_prod)
    mu2 = mu1
    sigma1 =  1/2 * np.std(industrcial_prod)
    sigma2 = 1.5 * np.std(industrcial_prod)

    starting_vals_hamilton = [p11, p22, mu1, mu2, sigma1, sigma2]
    # calculate initial neg_LL to see if we can improve over it
    neg_LL = NegativeLogLikelihood(parameter_vector = starting_vals_hamilton,
                                   y = industrcial_prod)
    print("--"*20)
    print(f"starting params:\t{starting_vals_hamilton}")
    print(f"Starting neg LL for Hamilton:\n{neg_LL}")
    print("--"*20)



    ################## (UN)CONSTRAINED OPTIMIZATION ###################
    # # TODO: This is not really needed to be honest, we can remove it.
    # # now do unconstrained and constrained minimization
    # # unconstrained optimization of the negative_LL
    # '''
    # comparison to matlab fminunc
    # https://nl.mathworks.com/help/optim/ug/fminunc.html
    # default_algo = Quasi-Newton
    # '''
    # print(f"\nUNCONSTRAINED OPTIMIZATION RESULTS")
    # print("--"*20)
    # # res_unc = minimize(NegativeLogLikelihood, x0=starting_vals_hamilton, method = "BFGS", args = Dy)
    # # BFGS fails here, so I tried Nelder-Mead
    # res_unc = minimize(NegativeLogLikelihood,
    #                    x0=starting_vals_hamilton,
    #                    method = "BFGS",
    #                    args = industrcial_prod)
    # ML_parameters_unc = res_unc.x
    # ML_NegativeLogL_unc = res_unc.fun
    #
    #
    # print(f"\nML parameters for unconstrained version:\n{ML_parameters_unc}")
    # print(f"\nML neg_LL for unconstrained version:\n{ML_NegativeLogL_unc}")



    # constrained optimization of the negative_LL
    '''
    comparison to matlab fmincon:
    https://nl.mathworks.com/help/optim/ug/fmincon.html
    default_algo = 'interior-point'
    '''
    print(f"\nCONSTRAINED OPTIMIZATION RESULTS")
    print("--"*20)
    # define bounds for each parameters
    bounds = ((0,1), (0,1), (-100,100), (-100, 100), (0,100), (0,100))

    # do a constrained minimization with a trust region (interior-point method) to
    # keep same logic as in matlab #trust-constr, SQLSP, L-BFGS-B
    res_con = minimize(NegativeLogLikelihood,
                       x0=starting_vals_hamilton,
                       method = "SLSQP",
                       args = industrcial_prod,
                       bounds = bounds)


    print(f"\nML parameter for constrained version:\n{res_con.x}")
    print(f"\nML neg_LL for constrained version:\n{res_con.fun}")

    # this function returns standard errors
    se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                                est_params = res_con.x,
                                args = industrcial_prod)
    df_constr_params = pd.DataFrame(index = ["p11", "p22", "mu1", "mu2", "sigma1", "sigm2"])
    df_constr_params["ML_est"] = res_con.x
    df_constr_params["ML_SE"] = se_ml_params
    print(f"\nThese are the 1a) results")
    print(df_constr_params)

    # get the smoothed values, since we have a MarkovSwitch model, we can extract
    # info from our predictions, since the states are dependent realizations
    smoothedxi = Hamilton_smoother(ML_estimators = res_con.x,
                                   y = industrcial_prod)

    p11 = res_con.x[0]
    p22 = res_con.x[1]
    mu1 = res_con.x[2]
    mu2 = res_con.x[3]
    sigma1 = res_con.x[4]
    sigma2 = res_con.x[5]

    # keep one here to get the filtered for the plot later, although not required
    filteredxi, predictedxi = Hamilton_filter(p11, p22, mu1, mu2, sigma1, sigma2, industrcial_prod)

    ############################## NOT IMPORTANT ###############################
    # # this is used for faster plotting, cuz I was annoyed re-araning args for
    # the optimization to run in a loop.
    #
    # df_smoothed = pd.DataFrame(columns = ["init1_1"], data = smoothedxi[1,:])
    # df_smoothed.to_pickle("smoothed_init_1.pkl")

    # add all smooths into a single dataframe such that everything is in 1 place
    # 1st load all the saved estimates (for various initializations, check
    # markov_switch_utils for the initialization codes)
    data_smoothed = pd.DataFrame()
    for i in range(1,4):
        df_temp = pd.read_pickle(f"smoothed_init_{i}.pkl")
        data_smoothed[f"init_{i}"] = df_temp.iloc[:,0]
    # print(data_smoothed)
    ############################################################################


    # Finally plot everything that is for part 1B)
    fig = plt.figure()
    # industrcial_prod.plot()
    plt.plot(industrcial_prod.index, np.log(data_smoothed.iloc[:,0]), label="smoothed_1_log")
    plt.plot(industrcial_prod.index, np.log(data_smoothed.iloc[:,1]), label="smoothed_2_log")
    plt.plot(industrcial_prod.index, np.log(data_smoothed.iloc[:,2]), label="smoothed_3_log")
    # plt.plot(industrcial_prod.index, np.log(filteredxi[1,:]), label="filtered_log")
    plt.ylabel("annual industrial growth")
    plt.savefig("images/qs_1b_logprob_smoothed.png")
    plt.legend()


    fig = plt.figure()
    # industrcial_prod.plot()
    plt.plot(industrcial_prod.index, data_smoothed.iloc[:,0], label="smoothed_1")
    plt.plot(industrcial_prod.index, data_smoothed.iloc[:,1], label="smoothed_2")
    plt.plot(industrcial_prod.index, data_smoothed.iloc[:,2], label="smoothed_3")
    # plt.plot(industrcial_prod.index, filteredxi[1,:], label="filtered")
    plt.ylabel("annual industrial growth")
    plt.legend()
    plt.savefig("images/qs_1b_norm_prob_smoothed.png")
    # plt.show()



    ############################################################################
    ################## EXTRA CONSTRAINED PROBABILITIES 1C) #####################
    ############################################################################
    # TODO: check this reasoning for now :0
    '''
    Lec 3 slide 14 and Lec 3 slide 16. We get a finite mixutre model. There the
    state realizations are an iid for each time t, hence there is no dependen on
    the past. Thus, we should expect a constant line in the smoother, only the
    last value should be the same as the filtered series. <-- That would be my guess

    If the smoother code is correct, i am pretty confident for the following code
    '''

    p11 = 0.2
    p22 = 0.8
    mu1 = np.mean(industrcial_prod)
    mu2 = mu1
    sigma1 =  1/2 * np.std(industrcial_prod)
    sigma2 = 2 * np.std(industrcial_prod)

    starting_vals_hamilton = [p11, p22, mu1, mu2, sigma1, sigma2]

    # constraints: the sum of probabilities should be the same
    def prob_constraint(starting_vals_hamilton):
        return starting_vals_hamilton[0] + starting_vals_hamilton[1]-1

    cons = ({'type': 'eq', 'fun': prob_constraint},)

    # do a constrained minimization with a trust region (interior-point method) to
    # keep same logic as in matlab #trust-constr, SQLSP, L-BFGS-B
    res_con = minimize(NegativeLogLikelihood,
                       x0=starting_vals_hamilton,
                       method = "SLSQP",
                       args = industrcial_prod,
                       bounds = bounds,
                       constraints = cons)


    print(f"\nML parameter for constrained version:\n{res_con.x}")
    print(f"\nML neg_LL for constrained version:\n{res_con.fun}")

    # calculate the standard errors as well
    se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                                est_params = res_con.x,
                                args = industrcial_prod)
    df_extraconstr_params = pd.DataFrame(index = ["p11", "p22", "mu1", "mu2", "sigma1", "sigm2"])
    df_extraconstr_params["ML_est_extra"] = res_con.x
    df_extraconstr_params["ML_SE_extra"] = se_ml_params
    print(f"\nExtra constrained parameters:")
    print(df_extraconstr_params)

    # get the smoothed extra constrained xi
    smoothedxi = Hamilton_smoother(ML_estimators = res_con.x,
                                   y = industrcial_prod)

    p11 = res_con.x[0]
    p22 = res_con.x[1]
    mu1 = res_con.x[2]
    mu2 = res_con.x[3]
    sigma1 = res_con.x[4]
    sigma2 = res_con.x[5]

    # keep one here to get the filtered for the plot later
    filteredxi, predictedxi = Hamilton_filter(p11, p22, mu1, mu2, sigma1, sigma2, industrcial_prod)

    fig = plt.figure()
    plt.plot(industrcial_prod.index, filteredxi[1,:], label = "filtered_extra")
    plt.plot(industrcial_prod.index, smoothedxi[1,:], label = "smoothed_extra")
    plt.legend()
    plt.savefig("images/qs_1c_filtered_vs_smoothe.png")
    plt.show()

    '''
    TODO:
    1. Check everything
    2. maybe do some extra plots for the writing part, since the teacher said
    he would give extra points for originality and not ismply following the
    instruction 1:1
    3. Fix the x-axis of the plots alter. plt.xticks([...])
    4. bonus :D --> make the cide shorter, I dont think it is worth the time thoug

    5. Continue with 1 d e after he gives a demo code for EM I guess :0
    '''

    ##################################################################################
    ################## 1D) - Initialisation with user-defined input ##################
    ##################################################################################
    
    # Set the starting values randomly
    iteration = 0
    T = industrcial_prod.shape[0]
    np.random.seed(1234)
    # p1star = np.random.uniform(0,1, size=(T))
    # p2star = 1 - p1star
    
    # p11star = np.multiply(np.random.uniform(0,1, size=(T)), p1star)
    # p22star = np.multiply(np.random.uniform(0,1, size=(T)), p2star)
    
    xi0_in = 0.5
    
    p11_init = 0.8
    p22_init = 0.8
    mu1_init = np.mean(industrcial_prod)
    mu2_init = mu1_init
    sigma1_init =  0.5 * np.std(industrcial_prod)
    sigma2_init = 1.5 * np.std(industrcial_prod)
    
    starting_vals_EM = [p11_init, p22_init, mu1_init, mu2_init, sigma1_init, sigma2_init, xi0_in]
    param_vector_EM = starting_vals_EM
    
    parameters_EM = []
    LogL_EM = []
    for k in range(200):
        iteration = iteration + 1
        
        p11,p22,mu1,mu2,sigma1,sigma2,xi0_out = EM_step_MS(parameter_vector= param_vector_EM, y= industrcial_prod)
        
        param_vector_EM = [p11,p22,mu1,mu2,sigma1,sigma2,xi0_out[0]]
        parameters_EM.append(param_vector_EM)
        
        EM_LogL = NegativeLogLikelihoodQ1d(param_vector_EM, industrcial_prod)
        LogL_EM.append(EM_LogL)
        
    df_em_params = pd.DataFrame(index = ["p11", "p22", "mu1", "mu2", "sigma1", "sigma2", "xi0_out"])
    df_extraconstr_params["ML_est_EM"] = parameters_EM[-1]
    print(f"\nEM parameters:")
    print(df_em_params)
    print(f"\LogL_EM:\n{LogL_EM[-1]}")
        
        
        
    
