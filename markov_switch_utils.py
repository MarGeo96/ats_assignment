import pandas as pd
import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt

def Hamilton_filter(p11, p22, mu1, mu2, sigma1, sigma2, y):
    '''
    Implementation of 2 state Hamilton filter from the Matlab Code
    Have checked it multiple times, it gives the same result as the MATLAB demo
    '''
    T = len(y)

    # build transition matrix from p11 and p22
    P = np.array([[p11, 1-p22],
                  [1-p11, p22]])



    # create all the storing matrices
    likelihood = np.empty((P.shape[0], T))
    filteredxi = np.empty((P.shape[0], T))
    predictedxi = np.empty((P.shape[0], T))

    # initialise by being in state 1???? <-- Lec 3 slide 46
    # first element is the unconditional probability of being in state 1
    # second element is the same for state 2
    predictedxi[:,0] = [(1-p22) / (2-p11-p22), (1-p11)/(2-p11-p22)]

    # initialise at state 1 for sure!
    # predictedxi[:,0] = np.dot(P, [1,0])
    # # initialise at state 2 for sure!
    # predictedxi[:,0] = np.dot(P, [0,1])


    for i in range(0, T):
        if i == 0:
            likelihood[:,i] = [norm.pdf(y[0], mu1, sigma1), norm.pdf(y[0], mu2, sigma2)]
            filteredxi[:,i] = np.multiply(predictedxi[:,i], likelihood[:,i]) / (np.dot(np.ones(shape = (1,2)), np.multiply(predictedxi[:,i],likelihood[:,i])))
            predictedxi[:,i+1]= np.dot(P, filteredxi[:,i])



        else:
            likelihood[:,i] = [norm.pdf(y[:i+1], mu1, sigma1)[-1], norm.pdf(y[:i+1], mu2, sigma2)[-1]]
            filteredxi[:,i] = np.multiply(predictedxi[:,i], likelihood[:,i]) / (np.dot(np.ones(shape = (1,2)), np.multiply(predictedxi[:,i],likelihood[:,i])))
            if i+1 != T:
                predictedxi[:,i+1]= np.dot(P, filteredxi[:,i])


    return filteredxi, predictedxi


def NegativeLogLikelihood(parameter_vector, y):
    '''
    this function will also apply the Hamilton Filter
    Checked multiple scenarios vs the MATLAB demo and it works fine
    '''

    p11 = parameter_vector[0]
    p22 = parameter_vector[1]
    mu1 = parameter_vector[2]
    mu2 = parameter_vector[3]
    sigma1 = parameter_vector[4]
    sigma2 = parameter_vector[5]

    # get the output of the Hamilton filter, aka the predictions
    filteredxi, predictedxi = Hamilton_filter(p11=p11,
                                              p22=p22,
                                              mu1=mu1,
                                              mu2=mu2,
                                              sigma1=sigma1,
                                              sigma2=sigma2,
                                              y=y)


    # collect the row vector of log likelihood per observation
    neg_LL = -1 * np.sum(np.log(np.multiply(predictedxi[0,:], norm.pdf(y, mu1, sigma1)) + np.multiply(predictedxi[1,:], norm.pdf(y, mu2, sigma2))))

    return neg_LL


def Hamilton_smoother(ML_estimators, y):
    '''
    TODO: check if it is correct:
    1. using the obtaiend optimal ML params, run a hamilton filter to get
    filtered and predicted xi.
    2. Create the transition matrix <- this should be just the optimal
    probabilities right? Just to double check :D
    3. Use the predicted xi to go backwards in time and produce the smoothed curve
    which relies on the relationships between the consecutive points. (Not
    applicable for Finite Mixutre model p11 + pp2 = 1)
    '''

    # extract the estimated params
    p11 = ML_estimators[0]
    p22 = ML_estimators[1]
    mu1 = ML_estimators[2]
    mu2 = ML_estimators[3]
    sigma1 = ML_estimators[4]
    sigma2 = ML_estimators[5]

    # run the optimal Hamilton filter to get the filtered and predicted xi
    filteredxi, predictedxi = Hamilton_filter(p11, p22, mu1, mu2, sigma1, sigma2, y)

    # construct the "estimated P" from the estimated params I guess???
    P = np.array([[ML_estimators[0], 1-ML_estimators[1]],
                  [1-ML_estimators[0], ML_estimators[1]]])


    # construct the saving matrix
    T = predictedxi.shape[1]
    smoothedxi = np.zeros(shape=predictedxi.shape)

    # go backwards in time. Set last obs to be same in all cases, cuz there is
    # no more future data.
    for i in range(T):
        t = T-i
        if t > T -1:
            # which1 should it be here? :D
            # smoothedxi[:,t-1] = predictedxi[:,T-1]
            smoothedxi[:,t-1] = filteredxi[:,T-1]
        else:
            # here I thought it is the other way around
            # smoothedxi[:,t-1] = np.multiply(filteredxi[:,t-1], np.dot(P.T, np.divide(predictedxi[:,t],filteredxi[:,t])))
            smoothedxi[:,t-1] = np.multiply(filteredxi[:,t-1], np.dot(P.T, np.divide(smoothedxi[:,t],predictedxi[:,t])))


    return smoothedxi

def filtered_plots(y, filteredxi, bool_plot = False):
    '''
    funciton to plot the filtered points and log/probabilities
    for being in state 1/2

    this is actually not really used I think
    '''

    fig = plt.figure()
    plt.scatter(x = range(len(y)),
                y = y,
                label = "Jump in infl")
    plt.plot(range(len(filteredxi[1,:])),
            filteredxi[1,:],
            label = 'prob high vol state',
            color = 'r')

    plt.legend()
    plt.savefig("images/infl_demo.png")



    fig = plt.figure()
    plt.scatter(x = range(len(y)),
                y = y,
                label = "Jump in infl")
    plt.plot(range(len(filteredxi[1,:])),
            np.log(filteredxi[1,:]),
            label = 'Logprob high vol state',
            color = 'r')
    plt.legend()
    plt.savefig("images/log_infl_demo.png")

    if bool_plot:
        plt.show()



###################################################################
############################### Q1d ###############################
###################################################################

def Hamilton_filterQ1d(p11, p22, mu1, mu2, sigma1, sigma2, xi0_in, y):
    T = len(y)

    # build transition matrix from p11 and p22
    P = np.array([[p11, 1-p22],
                  [1-p11, p22]])



    # create all the storing matrices
    likelihood = np.empty((P.shape[0], T))
    filteredxi = np.empty((P.shape[0], T))
    predictedxi = np.empty((P.shape[0], T))
    
    # initialise by using the user-defined input of the state at time zero
    predictedxi[:,0] = np.dot(P, xi0_in)
    
    for i in range(0, T):
        if i == 0:
            likelihood[:,i] = [norm.pdf(y[0], mu1, sigma1), norm.pdf(y[0], mu2, sigma2)]
            filteredxi[:,i] = np.multiply(predictedxi[:,i], likelihood[:,i]) / (np.dot(np.ones(shape = (1,2)), np.multiply(predictedxi[:,i],likelihood[:,i])))
            predictedxi[:,i+1]= np.dot(P, filteredxi[:,i])
        
        else:
            likelihood[:,i] = [norm.pdf(y[:i+1], mu1, sigma1)[-1], norm.pdf(y[:i+1], mu2, sigma2)[-1]]
            filteredxi[:,i] = np.multiply(predictedxi[:,i], likelihood[:,i]) / (np.dot( predictedxi[:,i],likelihood[:,i]))
            if i+1 != T:
                predictedxi[:,i+1]= np.dot(P, filteredxi[:,i])

    return filteredxi, predictedxi

def NegativeLogLikelihoodQ1d(parameter_vector, y):
    '''
    this function will also apply the Hamilton Filter
    Checked multiple scenarios vs the MATLAB demo and it works fine
    '''

    p11 = parameter_vector[0]
    p22 = parameter_vector[1]
    mu1 = parameter_vector[2]
    mu2 = parameter_vector[3]
    sigma1 = parameter_vector[4]
    sigma2 = parameter_vector[5]
    xi0_in = np.array([parameter_vector[6], 1-parameter_vector[6]])

    # get the output of the Hamilton filter, aka the predictions
    filteredxi, predictedxi = Hamilton_filterQ1d(p11=p11,
                                              p22=p22,
                                              mu1=mu1,
                                              mu2=mu2,
                                              sigma1=sigma1,
                                              sigma2=sigma2,
                                              xi0_in=xi0_in,
                                              y=y)


    # collect the row vector of log likelihood per observation
    neg_LL = -1 * np.sum(np.log(np.multiply(predictedxi[0,:], norm.pdf(y, mu1, sigma1)) + np.multiply(predictedxi[1,:], norm.pdf(y, mu2, sigma2))))

    return neg_LL

def Hamilton_smootherQ1d(ML_estimators, y):
    '''
    TODO: check if it is correct:
    1. using the obtaiend optimal ML params, run a hamilton filter to get
    filtered and predicted xi.
    2. Create the transition matrix <- this should be just the optimal
    probabilities right? Just to double check :D
    3. Use the predicted xi to go backwards in time and produce the smoothed curve
    which relies on the relationships between the consecutive points. (Not
    applicable for Finite Mixutre model p11 + pp2 = 1)
    '''

    # extract the estimated params
    p11 = ML_estimators[0]
    p22 = ML_estimators[1]
    mu1 = ML_estimators[2]
    mu2 = ML_estimators[3]
    sigma1 = ML_estimators[4]
    sigma2 = ML_estimators[5]
    xi0_in = np.array([ML_estimators[6], 1-ML_estimators[6]])


    # run the optimal Hamilton filter to get the filtered and predicted xi
    filteredxi, predictedxi = Hamilton_filterQ1d(p11, p22, mu1, mu2, sigma1, sigma2, xi0_in, y)
    

    
    # construct the "estimated P" from the estimated params I guess???
    P = np.array([[ML_estimators[0], 1-ML_estimators[1]],
                  [1-ML_estimators[0], ML_estimators[1]]])


    # construct the saving matrix
    T = predictedxi.shape[1]
    smoothedxi = np.empty(shape=predictedxi.shape)

    # go backwards in time. Set last obs to be same in all cases, cuz there is
    # no more future data.
    for i in range(T):
        t = T-i
        if t > T -1:
            # which1 should it be here? :D
            # smoothedxi[:,t-1] = predictedxi[:,T-1]
            smoothedxi[:,t-1] = filteredxi[:,T-1]
        else:
            # here I thought it is the other way around
            # smoothedxi[:,t-1] = np.multiply(filteredxi[:,t-1], np.dot(P.T, np.divide(predictedxi[:,t],filteredxi[:,t])))
            smoothedxi[:,t-1] = np.multiply(filteredxi[:,t-1], np.dot(P.T, np.divide(smoothedxi[:,t],predictedxi[:,t])))

    xi0_out = np.multiply(xi0_in, np.dot(P.T, np.divide(smoothedxi[:,0],predictedxi[:,0])))
    
    P_star = np.empty((T, P.shape[0], P.shape[1]))
    for i in range(T):
        if i == 0:         
            P_star[i,:,:] = np.divide(np.multiply(P, np.outer(smoothedxi[:,0], xi0_in)), np.outer(predictedxi[:,0], np.ones(shape = (2))))
        else:
            P_star[i,:,:] = np.divide(np.multiply(P, np.outer(smoothedxi[:,i], filteredxi[:,i-1])), np.outer(predictedxi[:,i], np.ones(shape = (2))))
    
    # smoothedxi = np.divide( smoothedxi, np.tile(np.sum(smoothedxi), (2,1)))
    
    # xi0_out = np.divide( xi0_out, np.tile(np.sum(xi0_out), (2,1)))
    
    # P_star = np.divide(P_star, np.sum(P_star))
    
    return smoothedxi, xi0_out, P_star

def EM_step_MS(parameter_vector, y):
    # T = y.shape[0]
    
    # p11 = parameter_vector[0]
    # p22 = parameter_vector[1]
    mu1 = parameter_vector[2]
    mu2 = parameter_vector[3]
    # sigma1 = parameter_vector[4]
    # sigma2 = parameter_vector[5]
    # xi0_in = np.array([parameter_vector[6], 1-parameter_vector[6]])
    
    smoothedxi, xi0_out, P_star = Hamilton_smootherQ1d(ML_estimators= parameter_vector, y= y)
    
    # print(P_star[:10,:,:])
    # print(xi0_out)
    # raise SystemExit
    
    p11star = P_star[:,0,0]
    p12star = P_star[:,0,1]
    p1star = p11star + p12star
    p21star = P_star[:,1,0]
    p22star = P_star[:,1,1]
    p2star = p21star + p22star
    


    p11_out = np.sum(p11star) / (xi0_out[0] + np.sum(p1star[:-1]))
    p22_out = np.sum(p22star) / (xi0_out[1] + np.sum(p2star[:-1]))
    mu1_out = np.divide( np.sum(np.multiply( p1star, y )), np.sum(p1star))
    mu2_out = np.divide(np.sum(np.multiply( p2star, y )), np.sum(p2star))
    sigma1_out = np.sqrt(np.divide(np.sum(np.multiply( p1star, np.power((y - mu1),2) )), np.sum(p1star)))
    sigma2_out = np.sqrt(np.divide(np.sum(np.multiply( p2star, np.power((y - mu2),2) )), np.sum(p2star)))
    

    
    
    return p11_out, p22_out, mu1_out, mu2_out, sigma1_out, sigma2_out, xi0_out

    
    

    
