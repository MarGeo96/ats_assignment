import numpy as np
import pandas as pd
from scipy.stats import norm
from scipy.optimize import minimize
from scipy.optimize import least_squares
import numdifftools as nd
import matplotlib.pyplot as plt

from state_space_utils import Kalman_Filter, Kalman_Smoother,\
                              NegativeLogLikelihood, state_space_prediction

import os
os.system('clear')


def calculate_SE(func, est_params, args):
    Hfun = nd.Hessian(func, method = "backward")
    h = Hfun(est_params, args)
    return np.sqrt(np.diag(np.linalg.inv(h)))

def estimate_AR1(parameter_vector, y):
    '''
    1. Minimize the negLL via scipy
    2. Do the Kalman_Filter with the best params
    3. Construct the param/SE error dataframe
    4. Execute the Kalman_Smoother
    '''

    # define bounds for each parameters
    bounds = ((0,1), (0,2), (0,2))

    # do a constrained minimization with a trust region (interior-point method) to
    # keep same logic as in matlab
    res_con = minimize(NegativeLogLikelihood,
                       x0=parameter_vector,
                       method = "SLSQP",
                       args = y,
                       bounds = bounds)
    ML_parameters_con = res_con.x
    ML_NegativeLogL_con = res_con.fun

    se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                             est_params = res_con.x,
                             args = y)

    df_constr_params = pd.DataFrame(index = ["phi", "sigma_eta_2", "sigma_epsilon_2"])
    df_constr_params["ML_est"] = res_con.x
    df_constr_params["ML_SE"] = se_ml_params

    # run optimal Kalman Filter
    xi, P, predictedxi, predictedP = Kalman_Filter(res_con.x, y)

    # Run the smoother
    smoothedxi, smoothedP = Kalman_Smoother(res_con.x, y = y)

    return xi, P, predictedxi, predictedP, smoothedxi, smoothedP, df_constr_params

if __name__ == '__main__':
    # load the data of annualized cons, employment and industrial growth
    # data = pd.read_csv("assignment/data/data.csv", index_col = "date")
    data = pd.read_csv("assignment/data/data_2019.csv", index_col = "date")

    # # This is for part 2b)
    # data = data.iloc[:-2]

    # demean the data
    data_means = data.mean()
    for idx, col in enumerate(data.columns):
        data[col] = data[col] - data_means[idx]
    print(f"Demeaned data:\{data}")

    # assing to seperate arrays
    y1t = data.iloc[:,0].values
    y2t = data.iloc[:,1].values
    y3t = data.iloc[:,2].values
    # remove this later
    # y4t = data.iloc[:,3].values

    startingvalues = [1, 1/3*np.var(y1t), 2/3*np.var(y1t)]

    # define bounds for each parameters
    bounds = ((-100,100), (0,100), (0,100))

    # do a constrained minimization with a trust region (interior-point method) to
    # keep same logic as in matlab
    res_con = minimize(NegativeLogLikelihood,
                       x0=startingvalues,
                       method = "SLSQP",
                       args = y1t,
                       bounds = bounds)
    ML_parameters_con = res_con.x
    ML_NegativeLogL_con = res_con.fun

    se_ml_params = calculate_SE(func = NegativeLogLikelihood,
                             est_params = res_con.x,
                             args = y1t)

    df_constr_params = pd.DataFrame(index = ["phi", "sigma_eta_2", "sigma_epsilon_2"])
    df_constr_params["ML_est"] = res_con.x
    df_constr_params["ML_SE"] = se_ml_params
    print(f"\nThe ML estimated params and their SE from the constrained optimization")
    print(df_constr_params)
    print(f"the obtained neg_LL is:\t{res_con.fun}")


    # get the filtered xi and P
    xi, P, predictedxi, predictedP = Kalman_Filter(parameter_vector = res_con.x,
                                                   y = y1t)
    # get the smoothed xi and P
    smoothedxi, smoothedP = Kalman_Smoother(parameter_vector = res_con.x,
                                            y = y1t)

    forecast_1 = state_space_prediction(step_prediction = 1,
                           y_observ = y1t,
                           est_xi = xi,
                           est_phi = res_con.x[0])

    forecast_4 = state_space_prediction(step_prediction = 4,
                           y_observ = y1t,
                           est_xi = xi,
                           est_phi = res_con.x[0])

    print("--"*20)
    print("Forecasting part QS 1")
    print(len(y1t))
    print(len(forecast_4))
    df_forecasts = pd.DataFrame(columns = ["y_obs", "y_pred_1", "y_pred_4"])
    df_forecasts["y_obs"] = y1t
    df_forecasts["y_pred_1"] = forecast_1
    df_forecasts["y_pred_4"] = forecast_4
    print(df_forecasts)
    print("The correlation between the forecasts is:\n")
    print(df_forecasts.corr())
