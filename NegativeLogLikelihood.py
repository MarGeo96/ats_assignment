import pandas as pd
import numpy as np
from scipy.stats import norm
from scipy.optimize import minimize
import numdifftools as nd
import matplotlib.pyplot as plt

from state_space_util_mdim import *

from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

import os
os.system('cls')

def NegativeLogLikelihood_MdimQ2e(F, Q, R, y):
    
    T = y.shape[0]
    ncol = y.shape[1]
    
    xi, P, predictedxi, predictedP = Kalman_Filter_MdimQ2e(F= F, Q= Q, R= R, y= y)

    
    mu = np.empty((T,ncol))
    LL = np.empty(T)

    for t in range(T):
        Sigma = predictedP[t] + R
        mu[t] = predictedxi[t,:]
        LL_1 = 1/np.sqrt(np.linalg.det(2*np.pi*Sigma))
        LL_2 = np.exp(-0.5 * np.dot(np.dot((y[t] - mu[t]).T, np.linalg.inv(Sigma)), (y[t] - mu[t])))
        dot_L = np.dot(LL_1, LL_2)
        LL[t] = np.log(dot_L)
    
    return -1 * np.sum(LL)



