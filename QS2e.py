import numpy as np
import pandas as pd
from scipy.stats import norm
from scipy.optimize import minimize
from scipy.optimize import least_squares
import numdifftools as nd
import matplotlib.pyplot as plt

from state_space_util_mdim import *
from EM_step import *
from NegativeLogLikelihood import *
import os
os.system('cls')


# def estimate_AR1(parameter_vector, y):
#     '''
#     1. Minimize the negLL via scipy
#     2. Do the Kalman_Filter with the best params
#     3. Construct the param/SE error dataframe
#     4. Execute the Kalman_Smoother
#     '''

#     # define bounds for each parameters
#     bounds = ((0,1), (0,2), (0,2))

#     # do a constrained minimization with a trust region (interior-point method) to
#     # keep same logic as in matlab
#     res_con = minimize(NegativeLogLikelihood,
#                        x0=parameter_vector,
#                        method = "SLSQP",
#                        args = y,
#                        bounds = bounds)
#     ML_parameters_con = res_con.x
#     ML_NegativeLogL_con = res_con.fun

#     se_ml_params = calculate_SE(func = NegativeLogLikelihood,
#                              est_params = res_con.x,
#                              args = y)

#     df_constr_params = pd.DataFrame(index = ["phi", "sigma_eta_2", "sigma_epsilon_2"])
#     df_constr_params["ML_est"] = res_con.x
#     df_constr_params["ML_SE"] = se_ml_params

#     # run optimal Kalman Filter
#     xi, P, predictedxi, predictedP = Kalman_Filter(res_con.x, y)

#     # Run the smoother
#     smoothedxi, smoothedP = Kalman_Smoother(res_con.x, y = y)

#     return xi, P, predictedxi, predictedP, smoothedxi, smoothedP, df_constr_params

if __name__ == '__main__':
    # load the data of annualized cons, employment and industrial growth
    # data = pd.read_csv("assignment/data/data.csv", index_col = "date")
    data = pd.read_csv("assignment/data/data_2019.csv", index_col = "date")

    # # # This is for part 2b)
    # # data = data.iloc[:-2]

    # # demean the data
    # data_means = data.mean()
    # for idx, col in enumerate(data.columns):
    #     data[col] = data[col] - data_means[idx]
    # print(f"Demeaned data:\{data}")

    # convert data to numpy matrix
    data = data.iloc[:-2, :]
    # # y = 100 * 2* (np.log(cpi.iloc[:,0]) - np.log(cpi.iloc[:,0].shift(1)))
    y = data - data.shift(1)
    y = y.iloc[:-1, ]
    y_cov = data.cov()
    # print(y)
    # exit()
    y_cov = y.cov()
    y_cov = y_cov.to_numpy()
    y = data.to_numpy()
    # print(y.shape)
    # exit()


    #######################################################################
    ######################### EM ALGO STUFF - Q2e #########################
    #######################################################################

    '''
    we have some obtained ML params. Now we want to run the EM algo.
    1. rewrite smoother for multivariate case
    2. write an EM step to optimize over F Q and R <- given in the demo code
    3. iterate and save the progress
    '''

    '''
    params to be estimated <- following prev year assignment
    f0, f1, f2, f3,
    r1, r2, r3,
    q1, q2, q3,
    h1=h2=h3 = 1 <- simply an identity matrix
    '''




    # F = 0.9 + 0.1 * np.random.rand(3,3)
    # Q = np.dot((1/2 + np.random.rand(3,3)) , 1/3 * y_cov)
    # R = np.dot((1/2 + np.random.rand(3,3)) , 2/3 * y_cov)
    # F = 0.9 * np.random.rand(3,3)

    F = np.identity(4) 
    Q = np.identity(4)
    R = np.identity(4)

    xi, P, predictedxi, predictedP = Kalman_Filter_MdimQ2e(F, Q, R, y)
    # exit()
    LL = NegativeLogLikelihood_MdimQ2e(F, Q, R, y)
    print(f"initial LL:\t{negLL}")

    smoothedxi, smoothedP, smoothedPcross, xi0_out, P0_out = Kalman_Smoother_MdimQ2e(F, Q, R, y)


    for k in range(1000):

        F, Q, R = EM_stepQ2e(F, Q, R, y)    

    print(f"\nF:\n{F}")
    print(f"\nQ:\n{Q}")
    print(f"\nR:\n{R}")

    LL = NegativeLogLikelihood_MdimQ2e(F, Q, R, y)
    print(f"LL:\t{negLL}")
